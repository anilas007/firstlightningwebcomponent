import { LightningElement,track } from 'lwc';

export default class FirstWebComponent extends LightningElement {
    heading = 'My Contact Card';
    
    @track
    firstName = 'Anil';
    @track
    lastName = 'Somasundaran';

}